#pragma once
#define _WINSOCKAPI_
#include "netlib/netlib.hpp"
#include <deque>
#include <string>
#include <cstdint>
#include <optional>
#include <memory>
#include "gdbserver.hpp"
#include "packet.hpp"

using Server = Net::TCP::Server;

class Console :public basic_read_buffer<char> {
    using Base = basic_read_buffer<char>;
    std::vector<char> buffer;
    HANDLE h_console_input;
public:
    Console();
    std::size_t read_from_source(std::back_insert_iterator<typename Base::Array> it) override;
    bool read_line(std::string& str);
};

class mDbg {
    PROCESS_INFORMATION pi;
    Server::ConnectionPtr child;
    Server::ConnectionPtr gdb;
    std::deque<std::string> response_packets;
    Console console;

    GDBServer gdb_server;

    mDbg() = default;
    bool inject_dll(const std::string& path);
    void resolv_dbg_events();
    void set_initial_break(std::size_t entry_point);

    void handle_commandline(std::string commandline);
    void handle_gdb_packet(Packet& packet);
    void handle_child_packet(Packet& packet);
    void handle_gdbserver_response(GDBServerResponse response);

    void ack();
    void nack();
public:
    static mDbg& get_instance();
    bool spawn_process(const std::string& exe_path, const std::string& child_dll);
    bool init_networking(uint16_t port);
    void run();
};

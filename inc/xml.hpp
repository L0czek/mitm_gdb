#pragma once

#include <string>

#define XML(version, data)  "<?xml version=\""#version"\"?>\n"\
							data
#define DOCTYPE(target) "<!DOCTYPE  target SYSTEM \""#target"\">\n"
#define TARGET(data) "<target>\n" \
                     data \
                     "</target>\n"
#define ARCHITECTURE(arch)  "<architecture>\n" \
                            #arch"\n" \
                            "</architecture>\n" 
#define OSABI(abi)  "<osabi>\n" \
                    #abi"\n" \
                    "</osabi>\n"
#define FEATURE(name, feature)      "<feature name=\""#name"\">\n" \
                                    feature \
                                    "</feature>\n"
#define FLAGS(id, size, data) "<flags id=\""#id"\" size=\""#size"\">\n" \
                              data \
                              "</flags>\n"
#define GET_MACRO(_1, _2, _3, NAME, ...) NAME
#define FIELD1(name, start, end) "<field name=\""#name"\" start=\""#start"\" end=\""#end"\"/>\n"
#define FIELD2(name, type) "<field name=\""#name"\" type=\""#type"\"/>\n"
#define FIELD(...) GET_MACRO(__VA_ARGS__, FIELD1, FIELD2)(__VA_ARGS__)
#define REG(name, bitsize, type, regnum) "<reg name=\""#name"\" bitsize=\""#bitsize"\" type=\""#type"\" regnum=\""#regnum"\"/>\n"
#define REG_GROUP(name, bitsize, type, regnum, group) "<reg name=\""#name"\" bitsize=\""#bitsize"\" type=\""#type"\" regnum=\""#regnum"\" group=\""#group"\"/>\n"
#define VECTOR(id, type, count) "<vector id=\""#id"\" type=\""#type"\" count=\""#count"\"/>\n"
#define UNION(id, data) "<union id=\""#id"\">\n" \
                        data \
                        "</union>\n"
#define THREADS(data) "<threads>\n", \
                      data, \
                      "</threads>"
#define EVAL(a) (a)

#include "arch.hpp"

const std::string& xml_target();
#pragma once
#define _WINSOCKAPI_
#include <string>
#include <cstdint>
#include <sstream>
#include <array>
#include <Windows.h>
#include <cstdarg>

#define BUFFER_SIZE 8192

using i8 = char;
using i16 = short;
using i32 = int;
using i64 = long long;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

#define DEFAULT_PIPE "\\\\.\\Pipe\\CTFPipe"

u32 getPid(const char* name);

class Pipe{
	HANDLE hPipe;
	std::array<u8, BUFFER_SIZE> buffer;
public:
	Pipe(const char* pipeName = DEFAULT_PIPE);
	void puts(const char* str) const;
	void printf(const char* format, ...) const;
	template<typename Type>
	const Pipe& operator<<(const Type& value) const{
		std::stringstream ss;
		ss << value;
		Pipe::puts(ss.str().c_str());
		return *this;
	}
};

void interactWithPipe(const char* name = DEFAULT_PIPE);
HANDLE injectDll(const char* processName, const char* dllName);
HANDLE injectDll(u32 pid, const char* dllName);
HANDLE spawnProcessSuspendedWithDll(const char* exeName, const char* dllName);

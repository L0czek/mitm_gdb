#pragma once

#include <string>
#include <tuple>
#include <sstream>
#include <optional>

template<typename... Elements>
class StringBuilder {
    std::tuple<Elements...> elements;
    template<std::size_t i=0>
    void iterate(std::stringstream& ss) {
        ss << std::get<i>(elements);
        if constexpr(i < (sizeof...(Elements) - 1)) {
            iterate<i+1>(ss);
        }
    }
public:
    StringBuilder(Elements&&... elements): elements(std::forward<Elements>(elements)...) {}
    std::string operator()() &&{
        std::stringstream ss;
        if constexpr(sizeof...(Elements) > 0) {
            iterate<0>(ss);
        }
        return ss.str();
    }
};

template<typename... Elements>
std::string concat(Elements&&... elements) {
    return StringBuilder<Elements...>{std::forward<Elements>(elements)...}();
}

template<typename Tc>
std::string hexlify(const Tc & tc) {
    const char * digits = "0123456789ABCDEF";
    std::string ret;
    for(const auto &i : tc) {
        ret += digits[(i & 0xF0) >> 4];
        ret += digits[(i & 0x0F) >> 0];
    }
    return ret;
}
inline std::string hexlify(char c) {
    const char * digits = "0123456789ABCDEF";
    std::string ret;
    ret += digits[(c & 0xF0) >> 4];
    ret += digits[(c & 0x0F) >> 0];
    return ret;
}
inline auto convert_char(char c) -> std::optional<char> {
    if(c >= '0' && c <= '9') {
        return c - '0';
    } else if(c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    } else if(c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    } else {
        return {};
    }
};
 
template<typename Tc>
bool unhexlify(const std::string& hex, Tc & tc) {
   if(hex.length() % 2 != 0) {
        return false;
    }
    auto it = std::back_inserter(tc);
    for(std::size_t i=0; i < hex.length(); i += 2) {
        auto chr_upper = convert_char(hex[i + 0]);
        auto chr_lower = conbert_char(hex[i + 1]);
        if(!chr_upper || !chr_lower) {
            return false;
        }
        it = (chr_upper << 4) | chr_lower;
    }
    return true;
}

template<typename Tc>
std::optional<Tc> unhexlify(const std::string& hex) {
    Tc tc;
    if(unhexlify(hex, tc)) {
        return tc;
    } else {
        return {};
    }
}

template<typename Tc>
Tc file_name(const Tc& tc) {
    std::size_t len = std::size(tc);
    if(len > 0) {
        std::size_t pos = len - 1;
        for(std::size_t i=0; i < len; i++, pos--) {
            if(tc[pos] == '\\' || tc[pos] == '/') {
                return Tc(std::begin(tc) + pos + 1, std::begin(tc) + len);
            }
        }
        return tc;
    } else {
        return Tc{};
    }
}
template<typename Func>
void for_each_in_split(const std::string& str, char delimeter, Func&& func) {
    std::stringstream ss;
    ss << str;
    std::string line;
    while(std::getline(ss, line, delimeter)) {
        std::invoke(func, std::move(line));
    }    
}

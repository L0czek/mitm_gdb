#pragma once
#define _WINSOCKAPI_
#include <windows.h>

#include <map>
#include <string>
#include <optional>

std::optional<std::map<std::string, std::size_t*>> parse_import_table(const std::string& dll_name); 
std::size_t get_entry_point(HANDLE h_process,const std::string& name);
std::pair<void*, void*> get_code_boundries(HANDLE h_process, void* ptr);


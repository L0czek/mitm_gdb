#pragma once

#include <chrono>
#include <thread>
#include <queue>
#include <memory>
#include <mutex>
#include <atomic>
#include <vector>
#include <functional>

#include "netlib/task.hpp"

namespace Task {
	template<typename _Base_Task>
	class basic_taskpool {
		std::queue<std::unique_ptr<_Base_Task>> tasks;
		std::mutex tasks_lock;
		std::atomic_bool exit_thread;
		std::vector<std::thread> threads;

		void thread_loop();
	public:
		basic_taskpool();
		~basic_taskpool();

		bool empty() const;
		void clear();
		void join_threads();
		void kill_threads();
		void resolve();
		void start(std::size_t n);

		template<typename _Task, typename... _Task_Init> void add_task(_Task_Init&&... Task_Init);
		template<typename... _Callbacks> void add_callback_list(_Callbacks&&... Callbacks);
	};

	using taskpool = basic_taskpool<Task>;

template<typename _Base_Task> basic_taskpool<_Base_Task>::basic_taskpool() {}
template<typename _Base_Task> basic_taskpool<_Base_Task>::~basic_taskpool() {
	kill_threads();
}
	
template<typename _Base_Task> bool basic_taskpool<_Base_Task>::empty() const {
	std::lock_guard<std::mutex> lock(tasks_lock);
	return tasks.empty();
}
template<typename _Base_Task> void basic_taskpool<_Base_Task>::clear() {
	std::lock_guard<std::mutex> lock(tasks_lock);
	std::move(tasks);
}
template<typename _Base_Task> void basic_taskpool<_Base_Task>::join_threads() {
	while(!threads.empty()){
		threads.back().join();
		threads.pop_back();
	}
}
template<typename _Base_Task> void basic_taskpool<_Base_Task>::kill_threads() {
	exit_thread = true;
	join_threads();
}
template<typename _Base_Task> void basic_taskpool<_Base_Task>::resolve() {
	for(;;) {
		std::unique_ptr<_Base_Task> task;
		if(exit_thread) {
			return;
		}
		{
			std::lock_guard<std::mutex> lock(tasks_lock);
			if(tasks.empty()) {
				return;
			}
			task = std::move(tasks.front());
			tasks.pop();
		}
		if(task != nullptr) {
			(*task)();
		}
	}
}
template<typename _Base_Task> void basic_taskpool<_Base_Task>::start(std::size_t n) {
	exit_thread = false;
	for(std::size_t i=0; i < n; i++) {
		threads.emplace_back(&basic_taskpool<_Base_Task>::thread_loop, this);
	}
}
using namespace std::chrono;
template<typename _Base_Task> void basic_taskpool<_Base_Task>::thread_loop() {
	for(;;) {
		if(exit_thread) {
			return;
		}
		resolve();
		std::this_thread::sleep_for(5ms);
	}
}
template<typename _Base_Task> 
template<typename _Task, typename... _Task_Init> void basic_taskpool<_Base_Task>::add_task(_Task_Init&&... Task_Init) {
	std::lock_guard<std::mutex> lock(tasks_lock);
	tasks.push(std::make_unique<_Task>(std::forward<_Task_Init>(Task_Init)...));
}
template<typename _Base_Task> 
template<typename... _Callbacks> void basic_taskpool<_Base_Task>::add_callback_list(_Callbacks&&... Callbacks) {
	std::lock_guard<std::mutex> lock(tasks_lock);
	tasks.push(std::make_unique<CallbackList<_Callbacks...>>(std::forward<_Callbacks>(Callbacks)...));
}

}
#pragma once

#include "netlib/endpoint.hpp"
#include "netlib/tcp_conn.hpp"

#include <memory>
#include <optional>

namespace Net {
	namespace TCP {
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		class basic_client {
			std::shared_ptr<_TaskPool> taskpool;
		public:
			typedef Connection<_IP, _Data_Type, _TaskPool> ConnectionType;
			typedef std::unique_ptr<ConnectionType> ConnectionPtr;
			typedef std::optional<ConnectionPtr> ConnOpt;
            typedef _TaskPool TaskPool;
            typedef std::shared_ptr<_TaskPool> TaskPoolPtr;
			
            basic_client();
			basic_client(std::shared_ptr<_TaskPool> taskpool);

			template<typename... _IP_Init>
			std::optional<ConnectionPtr> connect(_IP_Init&&... IP_Init);
			template<typename _Handler, typename... _IP_Init>
			void async_connect(const _Handler & Handler, _IP_Init&&... IP_Init);
			std::shared_ptr<_TaskPool> get_taskpool() const { return taskpool; }
		};

		using Client =  basic_client<Net::IP::V4, char, Task::taskpool>;

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		basic_client<_IP, _Data_Type, _TaskPool>::basic_client(): taskpool(std::make_shared<_TaskPool>()) {}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		basic_client<_IP, _Data_Type, _TaskPool>::basic_client(std::shared_ptr<_TaskPool> taskpool): taskpool(taskpool) {}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		template<typename _Handler,typename... _IP_Init>
		void basic_client<_IP, _Data_Type, _TaskPool>::async_connect(const _Handler & Handler, _IP_Init&&... IP_Init) {
			taskpool->template add_task<
				Task::AsyncTask<
					std::function<ConnOpt()>,
					std::function<void(ConnOpt)>
				>
			>([this, &IP_Init...](){ return this->connect(std::forward<_IP_Init>(IP_Init)...); }, Handler);
		}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		template<typename... _IP_Init>
		std::optional<typename basic_client<_IP, _Data_Type, _TaskPool>::ConnectionPtr> basic_client<_IP, _Data_Type, _TaskPool>::connect(_IP_Init&&... IP_Init) {
			Endpoint<_IP> remote{std::forward<_IP_Init>(IP_Init)...};
			int result = ::connect(remote.sockfd, reinterpret_cast<sockaddr*>(&remote.IP.addr_struct), sizeof(remote.IP.addr_struct));
#ifdef __linux__
			if(result == -1)
#else
			if(result == SOCKET_ERROR)
#endif	
				return {};
			return std::make_unique<ConnectionType>(remote, taskpool);		
		}
	}
}

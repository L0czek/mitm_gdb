#pragma once

#ifdef __linux__
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
#else
	#include <windows.h>
	#include <winsock2.h>
	#include <ws2tcpip.h>
#endif

#include "netlib/ip.hpp"

namespace Net{
	namespace TCP{
		template<typename _IP>
		struct Endpoint {
#ifdef __linux__
			int sockfd;
#else
			SOCKET sockfd;
#endif
			_IP IP;

			template<typename... _IP_Init>
			Endpoint(_IP_Init&&... IP_Init): IP(std::forward<_IP_Init>(IP_Init)...) {
				if constexpr(std::is_same<_IP, Net::IP::V4>::value) {
#ifdef __linux__
					sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
					if(sockfd < 0) {
						throw std::runtime_error("Error creating socket");
					}
#else
					sockfd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
					if(sockfd == INVALID_SOCKET) {
						throw std::runtime_error("Error creating socket");
					}
#endif
				} else {
					throw std::runtime_error("IPv6 currently not supported");
				}
			}

#ifdef __linux__
			template<typename... _IP_Init>
			Endpoint(int sockfd, _IP_Init&&... IP_Init): sockfd(sockfd), IP(std::forward<_IP_Init>(IP_Init)...) {}
#else
			template<typename... _IP_Init>
			Endpoint(SOCKET sockfd, _IP_Init&&... IP_Init): sockfd(sockfd), IP(std::forward<_IP_Init>(IP_Init)...) {}
#endif
		};
	}
}
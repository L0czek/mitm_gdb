#pragma once

#include "netlib/tcp_conn.hpp"

#include <unordered_set>
#include <memory>

namespace Net {
	namespace TCP {
		template<typename ConnectionType>
		class ConnectionSet {
			std::unordered_set<std::shared_ptr<ConnectionType>> connections;
		public:
			std::optional<std::shared_ptr<ConnectionType>> add(std::unique_ptr<ConnectionType> connection) {
				auto [ it, inserted] = connection.emplace(std::move(connection));
				if(inserted) {
					return *it;
				} else {
					return {};
				}
			}
			void remove(std::shared_ptr<ConnectionType> connection) {
				auto it = connections.find(connection);
				if(it != connections.end())
					connection.erase(it);
			}
			bool empty() const {
				return connections.empty();
			}
			void clear() {
				connections.clear();
			}
		};
	}
}
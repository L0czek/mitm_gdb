#pragma once

#include <string>
#include <cstring>
#include <cstdint>

#ifdef __linux__

#include <arpa/inet.h>

#else

#include <ws2tcpip.h>
#include <winsock2.h>

#endif

namespace Net{
	namespace IP{
		struct V4 {
			std::string addr;
			uint16_t port;
			struct sockaddr_in addr_struct;

			V4(const std::string& addr, uint16_t port): addr(addr), port(port) {
				memset(&addr_struct, 0, sizeof(addr_struct));
				addr_struct.sin_family = AF_INET;
				inet_pton(AF_INET, addr.c_str(), &addr_struct.sin_addr);
				addr_struct.sin_port = htons(port);
			}

			V4(const V4& ip): addr(ip.addr), port(ip.port) {
				memcpy(&addr_struct, &ip.addr_struct, sizeof(addr_struct));
			}

			V4(const struct sockaddr_in& remote) {
				memcpy(&addr_struct, &remote, sizeof(addr_struct));
				char buffer[0x10];
				inet_ntop(AF_INET, &addr_struct.sin_addr, buffer, 0x10);
				addr = buffer;
				port = ntohs(addr_struct.sin_port);
			}

			V4() {
				memset(&addr_struct, 0, sizeof(addr_struct));
				port = 0;
			}
		};
	}
}

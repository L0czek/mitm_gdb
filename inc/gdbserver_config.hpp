#pragma once

struct GDBServerConfig {
	bool replay_ack = true;
	std::vector<int> signals_to_deliver;
	bool non_stop_mode = false;
};
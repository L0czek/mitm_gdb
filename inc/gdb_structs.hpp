#pragma once

#include <list>
#include <string>
#include <vector>
#include "packet.hpp"

struct HardwareBreakpoint {
	bool used;
	std::size_t address;
	enum class Size {
		Byte = 0,
		Dword,
		Qword
	}size;
	enum class Trigger {
		Execute = 0,
		Write,
		Reserved,
		ReadOrWrite
	}trigger;
};

struct SoftwareBreakpoint {
	std::size_t address;
	char byte_backup;
};

struct Dll {
	HANDLE h_file;
	void* dll_base;
	void* debug_info;
	std::size_t debug_info_size;
};

struct Module {
	HMODULE handle;
	std::string path;
	std::string name;
	void* code_start;
	void* code_end;
};

struct Thread {
	HANDLE handle;
	void* thread_local_base;
	void* start;
};

struct StopEvent {
	DEBUG_EVENT event;
	DWORD pid;
	DWORD tid;
	CONTEXT ctx;
	char signal_code;
};

struct GDBServerResponse {
	std::vector<std::string> packets;
	GDBServerResponse();
	GDBServerResponse(GDBServerResponse&& response);
	GDBServerResponse(Packet&& packet);
	GDBServerResponse(std::string&& str);
	void operator+=(GDBServerResponse&& response);
	void operator+=(Packet&& packet);
	void operator+=(std::string&& str);
};

#define EMPTY_RESPONSE "$#00"
#define OK_RESPONSE Packet::gdb_general("OK");
#define ERR_RESPONSE(error) Packet::gdb_general("E"#error)
#define STRING_RESPONSE(str) Packet::gdb_general(str)
#define PARTIAL_RESPONSE(str, offset, size) Packet::gdb_partial(str, offset, size)
#define ACK_RESPONSE "+"
#define NACK_RESPONSE "-"

class GDBServer;
typedef GDBServerResponse (GDBServer::*HandlerRoutine)(std::string);

struct HandlerEntry {
	std::string packet_header;
	HandlerRoutine handler;
};



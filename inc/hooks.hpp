#pragma once
#define _WINSOCKAPI_
#include <Windows.h>
#include <cstdlib>
#include <optional>
#include <utility>

typedef struct _CLIENT_ID
{
     PVOID UniqueProcess;
     PVOID UniqueThread;
} CLIENT_ID, *PCLIENT_ID;

typedef PVOID PDBGUI_WAIT_STATE_CHANGE;
typedef PVOID POBJECT_ATTRIBUTES;
typedef PVOID PRTL_USER_PROCESS_PARAMETERS;
typedef PVOID PPROCESS_CREATE_INFO;
typedef PVOID PPROCESS_ATTRIBUTE_LIST;

NTSTATUS __stdcall dbg_ui_continue_hook(PCLIENT_ID AppClientId, NTSTATUS ContinueStatus);
NTSTATUS __stdcall dbg_ui_wait_state_change_hook(PDBGUI_WAIT_STATE_CHANGE StateChange, PLARGE_INTEGER Timeout);
NTSTATUS __stdcall dbg_ui_convert_state_change_structure_hook(PDBGUI_WAIT_STATE_CHANGE StateChange, struct _DEBUG_EVENT * DebugEvent);	

NTSTATUS __stdcall nt_create_user_process_hook(
	PHANDLE ProcessHandle,
	PHANDLE ThreadHandle,
	ACCESS_MASK ProcessDesiredAccess,
	ACCESS_MASK ThreadDesiredAccess,
	POBJECT_ATTRIBUTES ProcessObjectAttributes,
	POBJECT_ATTRIBUTES ThreadObjectAttributes,
	ULONG ProcessFlags,
	ULONG ThreadFlags,
	PRTL_USER_PROCESS_PARAMETERS ProcessParameters,
	PPROCESS_CREATE_INFO CreateInfo,
	PPROCESS_ATTRIBUTE_LIST AttributeList
	);

bool init_hooks();
bool patch(std::size_t* ptr, std::size_t value);
void enable_hooks();
void disable_hooks();
std::pair<HANDLE, HANDLE> get_process_handles();
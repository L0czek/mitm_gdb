#pragma once

#include <vector>
#include <string>
#include <map>
#include <optional>
#define _WINSOCKAPI_
#include <windows.h>
#include "gdbserver_config.hpp"

#define HARDWARE_BREAKPOINTS_NUM 4

#include "gdb_structs.hpp"

class GDBServer {
	enum class State {
		Initial,
		Running,
		Stopped
	}state = State::Initial;

	struct ProcessThreadId{
		DWORD process_id;
		DWORD thread_id;
	}current;

	struct ProcessThreadList {
		std::vector<DWORD> pids;
		std::vector<DWORD> tids;
	};

	struct {
		ProcessThreadList c;
		ProcessThreadList g;
	}process_thread_id;

	HardwareBreakpoint hbreaks[HARDWARE_BREAKPOINTS_NUM];
	std::vector<SoftwareBreakpoint> sbreaks;

	std::vector<Dll> dlls;
	std::map<DWORD, std::vector<Module>> modules;

	std::map<DWORD, Thread> threads;
	std::map<DWORD, HANDLE> processes;
	std::string serialized_threads;

	std::vector<HandlerEntry> handler_entries;
	
	GDBServerConfig config;

	std::deque<StopEvent> stop_events;

	// ------------------- Exception handling -------------------  
	GDBServerResponse handle_event(const EXCEPTION_DEBUG_INFO& event);
    GDBServerResponse handle_event(const CREATE_THREAD_DEBUG_INFO& event);
    GDBServerResponse handle_event(const CREATE_PROCESS_DEBUG_INFO& event);
    GDBServerResponse handle_event(const EXIT_THREAD_DEBUG_INFO& event);
    GDBServerResponse handle_event(const EXIT_PROCESS_DEBUG_INFO& event);
    GDBServerResponse handle_event(const LOAD_DLL_DEBUG_INFO& event);
    GDBServerResponse handle_event(const UNLOAD_DLL_DEBUG_INFO& event);
    GDBServerResponse handle_event(const OUTPUT_DEBUG_STRING_INFO& event);
    GDBServerResponse handle_event(const RIP_INFO& event);
    void parse_exception_record(GDBServerResponse& response, const EXCEPTION_RECORD& record);
    // ----------------------------------------------------------

    // ------------------- Packet handling ------------------- 
    GDBServerResponse handle_qSupported(std::string cmd);
    GDBServerResponse handle_vMustReplyEmpty(std::string cmd);
    GDBServerResponse handle_QStartNoAckMode(std::string cmd);
    GDBServerResponse handle_QProgramSignals(std::string cmd);
    GDBServerResponse handle_H(std::string cmd);
    GDBServerResponse handle_qXfer_features_read_target_xml(std::string cmd);
    GDBServerResponse handle_QNonStop(std::string cmd);
    GDBServerResponse handle_stop_reason(std::string cmd);
    GDBServerResponse handle_qXfer_threads_read(std::string cmd);
    GDBServerResponse handle_qXfer_exec_file_read(std::string cmd);
    GDBServerResponse handle_qAttached(std::string cmd);
    // ------------------------------------------------------

    // -------------------- Responses -----------------------
    GDBServerResponse generate_TStop(StopEvent event);
    // ------------------------------------------------------
    std::optional<std::pair<std::vector<DWORD>, std::vector<DWORD>>> parse_thread(std::string& str);
    GDBServerResponse generate_stop_event(char signal_code);
    std::optional<CONTEXT> get_thread_context(DWORD thread_id);
    void print_exceptions(const EXCEPTION_RECORD& record);
    HANDLE get_thread_handle(DWORD thread_id);
    HANDLE get_process_handle(DWORD process_id);
    std::optional<std::string> try_get_dll_name(void* ptr, bool unicode);
    std::vector<DWORD> get_pids(int pid);
    std::vector<DWORD> get_tids(int tid);
    void update_modules();
    std::string generate_thread_list();
public:
	GDBServer();
	GDBServerResponse handle_request(std::optional<std::string> cmd);
	GDBServerResponse poll_event();
	void set_process_thread_id(DWORD process_id, DWORD thread_id);

	bool set_hbreak(std::size_t address, 
					HardwareBreakpoint::Size size = HardwareBreakpoint::Size::Byte,
					HardwareBreakpoint::Trigger trigger = HardwareBreakpoint::Trigger::Execute);
	bool set_sbreak(std::size_t address);
	
	bool del_hbreak(std::size_t address);
	bool del_sbreak(std::size_t address);

	bool print_regs();
	GDBServerResponse handle_event(const DEBUG_EVENT& event);
};
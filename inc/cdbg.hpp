#pragma once
#define _WINSOCKAPI_
#include "netlib/netlib.hpp"
using Client = Net::TCP::Client;

class cDbg {
    Client::ConnectionPtr connection;

    cDbg() = default;
public:
    static cDbg& get_instance();
    bool init_networking();
    void debugger_main(HANDLE h_process, HANDLE h_thread, DWORD continue_status);
};

#pragma once

#include "log.hpp"
#include <string>
#include <Windows.h>
#include <type_traits>

#define RAX_INDEX 	0
#define RCX_INDEX 	1
#define RDX_INDEX 	2
#define RBX_INDEX 	3
#define RSP_INDEX 	4
#define RBP_INDEX 	5
#define RSI_INDEX 	6
#define RDI_INDEX 	7
#define R8_INDEX 	8
#define R9_INDEX 	9
#define R10_INDEX 	10
#define R11_INDEX 	11
#define R12_INDEX 	12
#define R13_INDEX 	13
#define R14_INDEX 	14
#define R15_INDEX 	15
#define RIP_INDEX 	16
#define EFLAGS	17

#define SEGCS_INDEX 18 
#define SEGSS_INDEX 19
#define SEGDS_INDEX 20
#define SEGES_INDEX 21
#define SEGFS_INDEX 22
#define SEGGS_INDEX 23


#define DR0_INDEX 
#define DR1_INDEX 
#define DR2_INDEX 
#define DR3_INDEX 
#define DR6_INDEX 
#define DR7_INDEX 

#define XMM0_INDEX 24
#define XMM1_INDEX 25
#define XMM2_INDEX 26
#define XMM3_INDEX 27
#define XMM4_INDEX 28
#define XMM5_INDEX 29
#define XMM6_INDEX 30
#define XMM7_INDEX 31
#define XMM8_INDEX 32
#define XMM9_INDEX 33
#define XMM10_INDEX 34
#define XMM11_INDEX 35
#define XMM12_INDEX 36
#define XMM13_INDEX 37
#define XMM14_INDEX 38
#define XMM15_INDEX 39


#define CF_INDEX 0
#define RESERVED_FLAG1 1
#define PF_INDEX 2
#define AF_INDEX 4
#define ZF_INDEX 6
#define SF_INDEX 7
#define TF_INDEX 8
#define IF_INDEX 9
#define DF_INDEX 10
#define OF_INDEX 11
#define NT_INDEX 14
#define RESERVED_FLAG2 15
#define RF_INDEX 16
#define VM_INDEX 17
#define AC_INDEX 18
#define VIF_INDEX 19
#define VIP_INDEX 20
#define ID_INDEX 21

#define REGISTER_INDEX_FROMAT "%02X"
#define REGISTER_FROMAT_64BIT "%016llX"
#define REGISTER_FROMAT_32BIT "%08X"
#define REGISTER_FROMAT_16BIT "%04X"

enum class Registers { 
	Rax = 0,
	Rcx,
	Rdx,
	Rbx,
	Rsp,
	Rbp,
	Rsi,
	Rdi,
	R8,
	R9,
	R10,
	R11,
	R12,
	R13,
	R14,
	R15,
	Rip,
	Eflags,
	Segcs,
	Segss,
	Segds,
	Seges,
	Segfs,
	Seggs,
	Xmm0,
	Xmm1,
	Xmm2,
	Xmm3,
	Xmm4,
	Xmm5,
	Xmm6,
	Xmm7,
	Xmm8,
	Xmm9,
	Xmm10,
	Xmm11,
	Xmm12,
	Xmm13,
	Xmm14,
	Xmm15,
	Number	
};

template<Registers reg> struct RegisterSize { static constexpr std::size_t bits = 64; typedef uint64_t type; };
template<> struct RegisterSize<Registers::Eflags> { static constexpr std::size_t bits = 32; typedef uint32_t type;};
template<> struct RegisterSize<Registers::Segcs> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Segss> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Segds> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Seges> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Segfs> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Seggs> { static constexpr std::size_t bits = 16; typedef uint16_t type; };
template<> struct RegisterSize<Registers::Xmm0> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm1> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm2> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm3> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm4> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm5> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm6> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm7> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm8> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm9> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm10> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm11> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm12> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm13> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm14> { static constexpr std::size_t bits = 128; }; 
template<> struct RegisterSize<Registers::Xmm15> { static constexpr std::size_t bits = 128; }; 

template<Registers reg>
const char * register_format() {
	constexpr std::size_t bits = RegisterSize<reg>::bits;
	if constexpr(bits == 64) {
		return REGISTER_INDEX_FROMAT ":" REGISTER_FROMAT_64BIT;
	} else if constexpr(bits == 32) {
		return REGISTER_INDEX_FROMAT ":" REGISTER_FROMAT_32BIT;
	} else if constexpr(bits == 16) {
		return REGISTER_INDEX_FROMAT ":" REGISTER_FROMAT_16BIT;
	} else if constexpr(bits == 128) {
		return REGISTER_INDEX_FROMAT ":" REGISTER_FROMAT_64BIT REGISTER_FROMAT_64BIT;
	} else 	{
		static_assert(false, "");
	}
}

template<Registers reg, Registers... List>
struct in_list {
	static constexpr bool value = std::disjunction<
   		std::is_same<
       		std::integral_constant<Registers, reg>,
        	std::integral_constant<Registers, List>
    	>...
    >::value;
};

template<Registers reg>
struct is_xmm {
	static constexpr bool value = in_list<reg, Registers::Xmm0, Registers::Xmm1, Registers::Xmm2, Registers::Xmm3, Registers::Xmm4, Registers::Xmm5, Registers::Xmm6, Registers::Xmm7, Registers::Xmm8, Registers::Xmm9, Registers::Xmm10, Registers::Xmm11, Registers::Xmm12, Registers::Xmm13, Registers::Xmm14, Registers::Xmm15>::value;
};

template<Registers reg>
std::string serialize_register(const CONTEXT& ctx) {
	char buffer[sizeof("00:00000000000000000000000000000000") + 1];
	if constexpr(is_xmm<reg>::value) {
		M128A value;
		switch(reg) {
			case Registers::Xmm0: value = ctx.Xmm0; break;
			case Registers::Xmm1: value = ctx.Xmm1; break;
			case Registers::Xmm2: value = ctx.Xmm2; break;
			case Registers::Xmm3: value = ctx.Xmm3; break;
			case Registers::Xmm4: value = ctx.Xmm4; break;
			case Registers::Xmm5: value = ctx.Xmm5; break;
			case Registers::Xmm6: value = ctx.Xmm6; break;
			case Registers::Xmm7: value = ctx.Xmm7; break;
			case Registers::Xmm8: value = ctx.Xmm8; break;
			case Registers::Xmm9: value = ctx.Xmm9; break;
			case Registers::Xmm10: value = ctx.Xmm10; break;
			case Registers::Xmm11: value = ctx.Xmm11; break;
			case Registers::Xmm12: value = ctx.Xmm12; break;
			case Registers::Xmm13: value = ctx.Xmm13; break;
			case Registers::Xmm14: value = ctx.Xmm14; break;
			case Registers::Xmm15: value = ctx.Xmm15; break;
		}
		if(std::snprintf(
			buffer,
			sizeof(buffer), 
			register_format<reg>(), 
			static_cast<unsigned char>(reg), 
			value.High,
			value.Low) <= 0) {
			error(THIS_PLACE, "sprintf error");
			return "";
		} else {
			return std::string(buffer);
		}
	} else {
		typename RegisterSize<reg>::type value;
		switch(reg) {
			case Registers::Rax: value = static_cast<decltype(value)>(ctx.Rax); break;
			case Registers::Rcx: value = static_cast<decltype(value)>(ctx.Rcx); break;
			case Registers::Rdx: value = static_cast<decltype(value)>(ctx.Rdx); break;
			case Registers::Rbx: value = static_cast<decltype(value)>(ctx.Rbx); break;
			case Registers::Rsp: value = static_cast<decltype(value)>(ctx.Rsp); break;
			case Registers::Rbp: value = static_cast<decltype(value)>(ctx.Rbp); break;
			case Registers::Rsi: value = static_cast<decltype(value)>(ctx.Rsi); break;
			case Registers::Rdi: value = static_cast<decltype(value)>(ctx.Rdi); break;
			case Registers::R8: value = static_cast<decltype(value)>(ctx.R8); break;
			case Registers::R9: value = static_cast<decltype(value)>(ctx.R9); break;
			case Registers::R10: value = static_cast<decltype(value)>(ctx.R10); break;
			case Registers::R11: value = static_cast<decltype(value)>(ctx.R11); break;
			case Registers::R12: value = static_cast<decltype(value)>(ctx.R12); break;
			case Registers::R13: value = static_cast<decltype(value)>(ctx.R13); break;
			case Registers::R14: value = static_cast<decltype(value)>(ctx.R14); break;
			case Registers::R15: value = static_cast<decltype(value)>(ctx.R15); break;
			case Registers::Rip: value = static_cast<decltype(value)>(ctx.Rip); break;
			case Registers::Eflags: value = static_cast<decltype(value)>(ctx.EFlags); break;
			case Registers::Segcs: value = static_cast<decltype(value)>(ctx.SegCs); break;
			case Registers::Segss: value = static_cast<decltype(value)>(ctx.SegSs); break;
			case Registers::Segds: value = static_cast<decltype(value)>(ctx.SegDs); break;
			case Registers::Seges: value = static_cast<decltype(value)>(ctx.SegEs); break;
			case Registers::Segfs: value = static_cast<decltype(value)>(ctx.SegFs); break;
			case Registers::Seggs: value = static_cast<decltype(value)>(ctx.SegGs); break;
			//default: static_assert(false, "");
		}
		if(std::snprintf(
			buffer,
			sizeof(buffer), 
			register_format<reg>(), 
			static_cast<unsigned char>(reg), 
			value) <= 0) {
			error(THIS_PLACE, "sprintf error");
			return "";
		} else {
			return std::string(buffer);
		}
	}
}
std::string serialize_pid_tid(DWORD pid, DWORD tid);
std::string serialize_registers(const CONTEXT& ctx);
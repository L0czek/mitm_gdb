#pragma once

#include "common.hpp"
#include <ctime>

#define _WINSOCKAPI_
#include <windows.h>

template<typename... Elements>
void error(Elements&&... elements) {
	auto now = time(NULL);
	auto tm = localtime(&now);
	char buffer[0x100];
	strftime(buffer, sizeof(buffer), "%c", tm);
	fprintf(stderr, "%s :[", buffer);
	auto handle = GetStdHandle(STD_ERROR_HANDLE);
	SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_INTENSITY);
	fprintf(stderr, "ERROR");
	SetConsoleTextAttribute(handle, 15);
	fprintf(stderr, "]: %s\n", concat(std::forward<Elements>(elements)...).c_str());
}
template<typename... Elements>
void info(Elements&&... elements) {
	auto now = time(NULL);
	auto tm = localtime(&now);
	char buffer[0x100];
	strftime(buffer, sizeof(buffer), "%c", tm);
	fprintf(stderr, "%s :[", buffer);
	auto handle = GetStdHandle(STD_ERROR_HANDLE);
	SetConsoleTextAttribute(handle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	fprintf(stderr, "INFO");
	SetConsoleTextAttribute(handle, 15);
	fprintf(stderr, "]: %s\n", concat(std::forward<Elements>(elements)...).c_str());
}
template<typename... Elements>
void warning(Elements&&... elements) {
	auto now = time(NULL);
	auto tm = localtime(&now);
	char buffer[0x100];
	strftime(buffer, sizeof(buffer), "%c", tm);
	fprintf(stderr, "%s :[", buffer);
	auto handle = GetStdHandle(STD_ERROR_HANDLE);
	SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	fprintf(stderr, "WARNING");
	SetConsoleTextAttribute(handle, 15);
	fprintf(stderr, "]: %s\n", concat(std::forward<Elements>(elements)...).c_str());
}

#define THIS_PLACE __FILE__, ":", __LINE__, " in ", __FUNCSIG__


std::string last_error();
#pragma once

#include "netlib/netlib.hpp"
#include <string>
#include <optional>

using Server = Net::TCP::Server;

class Packet {
public:
    enum class Type {
        GDB,
        MGDB,
        Ack,
        NAck,
        None
    };

private:
    Type type;
    char type_char;

    enum class StateOfRecv {
        Empty,
        Content,
        Checksum1,
        Checksum2,
        Finished
    }state;

    std::string content;
    char checksum;

public:
    Packet();
    Packet(char type_char, std::string content);
    char calc_checksum() const;
    bool receive(Server::ConnectionPtr& connection);
    std::optional<std::string> get_content();
    bool is_valid() const;
    std::string to_string();
    void clear();
    Type get_type() const;

    static Packet gdb_general(std::string content);
    static Packet gdb_notification(std::string content);
    static Packet gdb_partial(const std::string& content, std::size_t offset, std::size_t size);
};

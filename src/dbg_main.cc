#include <iostream>
#include "mdbg.hpp"
#include "ctf.hpp"
#include "hooks.hpp"
#include "cdbg.hpp"
#include "log.hpp"

int main() {
    if(!mDbg::get_instance().spawn_process("test.exe","cdbg.dll")) {
        puts("Process not spawned");
        exit(-1);
    }
    try{
        if(!mDbg::get_instance().init_networking(12345)) {
            error("Network not initialised");
            exit(-1);
        }
    }catch(std::runtime_error e) {
         error(e.what(), last_error());
         exit(-1);
    }
    mDbg::get_instance().run();
}

#include "gdbserver.hpp"

#include <algorithm>
#include <iterator>
#include <csignal>
#define SIGTRAP 5
#define SIGSTOP 19
#include <iostream>
#include <utility>
#include <psapi.h>

#include "packet.hpp"
#include "log.hpp"
#include "xml.hpp"
#include "arch.hpp"
#include "pe.hpp"


GDBServer::GDBServer(): handler_entries({
	{ "qSupported", &GDBServer::handle_qSupported },
	{ "vMustReplyEmpty", &GDBServer::handle_vMustReplyEmpty },
	{ "QStartNoAckMode", &GDBServer::handle_QStartNoAckMode },
	{ "QProgramSignals", &GDBServer::handle_QProgramSignals },
	{ "H", &GDBServer::handle_H },
	{ "qXfer:features:read:target.xml:", &GDBServer::handle_qXfer_features_read_target_xml },
	{ "QNonStop:", &GDBServer::handle_QNonStop },
	{ "?", &GDBServer::handle_stop_reason },
	{ "qXfer:threads:read::", &GDBServer::handle_qXfer_threads_read },
	{ "qXfer:exec-file:read:", &GDBServer::handle_qXfer_exec_file_read },
	{ "qAttached", &GDBServer::handle_qAttached }
}) {

}

GDBServerResponse GDBServer::handle_request(std::optional<std::string> cmd) {
	GDBServerResponse response;
	if(!cmd) {
		if(config.replay_ack) {
			response += NACK_RESPONSE;
		} else {
			response += EMPTY_RESPONSE;
		}
	} else {
		if(config.replay_ack) {
			response += ACK_RESPONSE;
		}
		auto msg = std::move(*cmd);
		auto it = std::find_if(handler_entries.begin(), handler_entries.end(), [&msg](const HandlerEntry& entry){
			return msg.find(entry.packet_header) == 0;
		});
		if(it != handler_entries.end()) {
			msg.erase(0, it->packet_header.length());
			response += std::invoke(it->handler, this, std::move(msg));
		} else {
			error("Packet `", msg, "` not supported");
			response += EMPTY_RESPONSE;
		}
	}
	return response;
}

GDBServerResponse GDBServer::poll_event() {

	return {};
}

void GDBServer::set_process_thread_id(DWORD process_id, DWORD thread_id) {
	current.process_id = process_id;
	current.thread_id = thread_id;
}

bool GDBServer::set_hbreak(std::size_t address, HardwareBreakpoint::Size size, HardwareBreakpoint::Trigger trigger) {
	for(std::size_t i=0; i < HARDWARE_BREAKPOINTS_NUM; ++i) {
		if(!hbreaks[i].used) {
			CONTEXT ctx;
			ctx.ContextFlags = CONTEXT_DEBUG_REGISTERS;
			if(!GetThreadContext(get_thread_handle(current.thread_id), &ctx)) {
				error(THIS_PLACE, " GetThreadContext ", get_thread_handle(current.thread_id), " handle ", last_error());
				return false;
			}
			switch(i) {
				case 0: ctx.Dr0 = address; break;
				case 1: ctx.Dr1 = address; break;
				case 2: ctx.Dr2 = address; break;
				case 3: ctx.Dr3 = address; break;
			}
			ctx.Dr7 &= (~(3 << (2*i))) & (~(3 << (16+i*4))) & (~(3 << (18 + i*4)));
			ctx.Dr7 |= (i * 2 + 1) // global enable (1, 3, 5, 7) bits
					|  static_cast<std::size_t>(trigger) << (16 + i * 4) // trigger 16-17 20-21 ...
					|  static_cast<std::size_t>(size) << (18 + i * 4); // size
			if(!SetThreadContext(get_thread_handle(current.thread_id), &ctx)) {
				error(THIS_PLACE, " SetThreadContext ", get_thread_handle(current.thread_id), " handle ", last_error());
				return false;
			}
			hbreaks[i].used = true;
			hbreaks[i].address = address;
			hbreaks[i].trigger = trigger;
			hbreaks[i].size = size;
			return true;
		}
	}
	return false;
}
bool GDBServer::set_sbreak(std::size_t address) {
	return true;
}
	
bool GDBServer::del_hbreak(std::size_t address) {
	return true;
}
bool GDBServer::del_sbreak(std::size_t address) {
	return true;
}
HANDLE GDBServer::get_thread_handle(DWORD thread_id) {
	auto it = threads.find(thread_id);
	if(it == threads.end()) {
		warning("Attempting to get handle to new thread_id ", thread_id);
		auto handle = OpenThread(THREAD_ALL_ACCESS, 0, thread_id);
		if(handle == INVALID_HANDLE_VALUE) {
			error("Cannot open handle to thread ", thread_id);
			return INVALID_HANDLE_VALUE;
		} else {
			threads.insert(std::make_pair(thread_id, Thread{
				handle,
				NULL,
				NULL
			}));
			return handle;
		}
	} else {
		return it->second.handle;
	}
}
HANDLE GDBServer::get_process_handle(DWORD process_id) {
	auto it = processes.find(process_id);
	if(it == processes.end()) {
		warning("Attempting to get handle to new process_id ", process_id);
		auto handle = OpenProcess(PROCESS_ALL_ACCESS, 0, process_id);
		if(handle == INVALID_HANDLE_VALUE) {
			error("Cannot open handle to process ", process_id);
			return INVALID_HANDLE_VALUE;
		} else {
			processes.insert(std::make_pair(process_id, handle));
			return handle;
		}
	} else {
		return it->second;
	}
}
bool GDBServer::print_regs() {
	CONTEXT ctx;
	ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_DEBUG_REGISTERS;
	if(!GetThreadContext(get_thread_handle(current.thread_id), &ctx)) {
		error(THIS_PLACE, " GetThreadContext ", get_thread_handle(current.thread_id), " handle ", last_error());
		return false;
	}
	puts("---------------------------------- REGS DUMP ----------------------------------");
	printf("rax => 0x%016llX\n"  
    	   "rcx => 0x%016llX\n"  
    	   "rdx => 0x%016llX\n"  
    	   "rbx => 0x%016llX\n"  
    	   "rsp => 0x%016llX\n"  
    	   "rbp => 0x%016llX\n"  
    	   "rsi => 0x%016llX\n"  
    	   "rdi => 0x%016llX\n"  
    	   "r8  => 0x%016llX\n"  
    	   "r9  => 0x%016llX\n"  
    	   "r10 => 0x%016llX\n"  
    	   "r11 => 0x%016llX\n"  
    	   "r12 => 0x%016llX\n"  
    	   "r13 => 0x%016llX\n"  
    	   "r14 => 0x%016llX\n"  
    	   "r15 => 0x%016llX\n"  
    	   "rip => 0x%016llX\n"  
    	   "dr0 => 0x%016llX\n"  
    	   "dr1 => 0x%016llX\n"  
    	   "dr2 => 0x%016llX\n"  
    	   "dr3 => 0x%016llX\n"  
    	   "dr6 => 0x%016llX\n"  
    	   "dr7 => 0x%016llX\n",
    	   ctx.Rax, ctx.Rcx, ctx.Rdx, ctx.Rbx, ctx.Rsp, ctx.Rbp, ctx.Rsi, ctx.Rdi, ctx.R8, 
    	   ctx.R9, ctx.R10, ctx.R11, ctx.R12, ctx.R13, ctx.R14, ctx.R15, ctx.Rip, ctx.Dr0, 
    	   ctx.Dr1, ctx.Dr2, ctx.Dr3, ctx.Dr6, ctx.Dr7);
    return true;
}

GDBServerResponse GDBServer::handle_event(const DEBUG_EVENT& event) {
	set_process_thread_id(event.dwProcessId, event.dwThreadId);
	switch(event.dwDebugEventCode) {
		case EXCEPTION_DEBUG_EVENT: 		return handle_event(event.u.Exception); break;
		case EXIT_PROCESS_DEBUG_EVENT: 		return handle_event(event.u.ExitProcess); break;
		case CREATE_THREAD_DEBUG_EVENT: 	return handle_event(event.u.CreateThread); break;
		case EXIT_THREAD_DEBUG_EVENT: 		return handle_event(event.u.ExitThread); break;
		case CREATE_PROCESS_DEBUG_EVENT: 	return handle_event(event.u.CreateProcessInfo); break;
		case LOAD_DLL_DEBUG_EVENT: 			return handle_event(event.u.LoadDll); break;
		case UNLOAD_DLL_DEBUG_EVENT: 		return handle_event(event.u.UnloadDll); break;
	}
}

GDBServerResponse GDBServer::handle_event(const EXCEPTION_DEBUG_INFO& event) {
	GDBServerResponse response;
	parse_exception_record(response, event.ExceptionRecord);
	return response;
}
void GDBServer::parse_exception_record(GDBServerResponse& response, const EXCEPTION_RECORD& record) {
	char signal_code;
	switch(record.ExceptionCode) {
		case EXCEPTION_ACCESS_VIOLATION: 
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED: 
		case EXCEPTION_STACK_OVERFLOW: 
		case EXCEPTION_DATATYPE_MISALIGNMENT: 
		case EXCEPTION_IN_PAGE_ERROR:
			signal_code = SIGSEGV;
			break; 
		case EXCEPTION_FLT_DENORMAL_OPERAND: 
		case EXCEPTION_FLT_DIVIDE_BY_ZERO: 
		case EXCEPTION_FLT_INEXACT_RESULT: 
		case EXCEPTION_FLT_INVALID_OPERATION: 
		case EXCEPTION_FLT_OVERFLOW: 
		case EXCEPTION_FLT_STACK_CHECK: 
		case EXCEPTION_FLT_UNDERFLOW: 
		case EXCEPTION_INT_DIVIDE_BY_ZERO: 
		case EXCEPTION_INT_OVERFLOW:
			signal_code = SIGFPE;
			break; 
		case EXCEPTION_ILLEGAL_INSTRUCTION: 
		case EXCEPTION_PRIV_INSTRUCTION: 
			signal_code = SIGILL;
			break;
		case EXCEPTION_SINGLE_STEP: 
		case EXCEPTION_BREAKPOINT:
			signal_code = SIGTRAP;
			break; 
		case EXCEPTION_INVALID_DISPOSITION:  // have no idea how to categorise these 2
		case EXCEPTION_NONCONTINUABLE_EXCEPTION:
			signal_code = SIGSTOP;
			break; 
	}
	response += generate_stop_event(signal_code);
	print_exceptions(record);
	if(record.ExceptionRecord) {
		parse_exception_record(response, *record.ExceptionRecord);
	}
}
GDBServerResponse GDBServer::generate_stop_event(char signal_code) {
	CONTEXT ctx;
	ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_DEBUG_REGISTERS;
	if(!GetThreadContext(get_thread_handle(current.thread_id), &ctx)) {
		error(THIS_PLACE, " cannot get thread context TID(", current.thread_id, ") HANDLE(", get_thread_handle(current.thread_id),") ", last_error());
		return {};
	}
	if(state == GDBServer::State::Running) {
		return generate_TStop(stop_events.front()); 
	} else {
		StopEvent event;
		event.ctx = ctx;
		event.pid = current.process_id;
		event.tid = current.thread_id;
		event.signal_code = signal_code;
		stop_events.push_back(event);
		info("StopEvent for thread TID(", current.thread_id, ") added to queue");
		return {};
	}
}
std::optional<CONTEXT> GDBServer::get_thread_context(DWORD thread_id) {
	CONTEXT ctx;
	ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_DEBUG_REGISTERS;
	if(!GetThreadContext(get_thread_handle(thread_id), &ctx)) {
		error("Cannot get context of thread TID(", thread_id, ")  ", last_error());
		return {};
	}
	return ctx;
}
void GDBServer::update_modules() {
	modules.clear();
	for(const auto process : processes){
		HMODULE hmods[1024];
		DWORD cb_needed;
		HANDLE h_process = process.second;
		if(EnumProcessModules(h_process, hmods, sizeof(hmods), &cb_needed)) {
			for(std::size_t i=0; i < (cb_needed / sizeof(HMODULE)); ++i) {
				char buffer[MAX_PATH];
				if(GetModuleFileNameExA(h_process, hmods[i], buffer, sizeof(buffer))) {
					auto [ code_begin, code_end] = get_code_boundries(h_process, hmods[i]);
					auto path = std::string(buffer);
					std::string name;
					auto pos = path.find_last_of('\\');
					if(pos == std::string::npos) {
						name = path;
					} else {
						name = path.substr(pos + 1);
					}
					info("Updating module ", hmods[i], " name(", name,") code between ", code_begin, ":", code_end);
					modules[process.first].push_back(Module{
						hmods[i],
						path,
						name,
						code_begin,
						code_end
					});
				}
			}
		}
	}
}
void GDBServer::print_exceptions(const EXCEPTION_RECORD& record) {
	switch(record.ExceptionCode) {
		case EXCEPTION_ACCESS_VIOLATION: info("Thread TID(", current.thread_id, ") received EXCEPTION_ACCESS_VIOLATION"); break;
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED: info("Thread TID(", current.thread_id, ") received EXCEPTION_ARRAY_BOUNDS_EXCEEDED"); break;
		case EXCEPTION_BREAKPOINT: info("Thread TID(", current.thread_id, ") received EXCEPTION_BREAKPOINT"); break;
		case EXCEPTION_DATATYPE_MISALIGNMENT: info("Thread TID(", current.thread_id, ") received EXCEPTION_DATATYPE_MISALIGNMENT"); break;
		case EXCEPTION_FLT_DENORMAL_OPERAND: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_DENORMAL_OPERAND"); break;
		case EXCEPTION_FLT_DIVIDE_BY_ZERO: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_DIVIDE_BY_ZERO"); break;
		case EXCEPTION_FLT_INEXACT_RESULT: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_INEXACT_RESULT"); break;
		case EXCEPTION_FLT_INVALID_OPERATION: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_INVALID_OPERATION"); break;
		case EXCEPTION_FLT_OVERFLOW: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_OVERFLOW"); break;
		case EXCEPTION_FLT_STACK_CHECK: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_STACK_CHECK"); break;
		case EXCEPTION_FLT_UNDERFLOW: info("Thread TID(", current.thread_id, ") received EXCEPTION_FLT_UNDERFLOW"); break;
		case EXCEPTION_ILLEGAL_INSTRUCTION: info("Thread TID(", current.thread_id, ") received EXCEPTION_ILLEGAL_INSTRUCTION"); break;
		case EXCEPTION_IN_PAGE_ERROR: info("Thread TID(", current.thread_id, ") received EXCEPTION_IN_PAGE_ERROR"); break;
		case EXCEPTION_INT_DIVIDE_BY_ZERO: info("Thread TID(", current.thread_id, ") received EXCEPTION_INT_DIVIDE_BY_ZERO"); break;
		case EXCEPTION_INT_OVERFLOW: info("Thread TID(", current.thread_id, ") received EXCEPTION_INT_OVERFLOW"); break;
		case EXCEPTION_INVALID_DISPOSITION: info("Thread TID(", current.thread_id, ") received EXCEPTION_INVALID_DISPOSITION"); break;
		case EXCEPTION_NONCONTINUABLE_EXCEPTION: info("Thread TID(", current.thread_id, ") received EXCEPTION_NONCONTINUABLE_EXCEPTION"); break;
		case EXCEPTION_PRIV_INSTRUCTION: info("Thread TID(", current.thread_id, ") received EXCEPTION_PRIV_INSTRUCTION"); break;
		case EXCEPTION_SINGLE_STEP: info("Thread TID(", current.thread_id, ") received EXCEPTION_SINGLE_STEP"); break;
		case EXCEPTION_STACK_OVERFLOW: info("Thread TID(", current.thread_id, ") received EXCEPTION_STACK_OVERFLOW"); break;
	}
}
GDBServerResponse GDBServer::handle_event(const CREATE_THREAD_DEBUG_INFO& event) {
	threads.insert(std::make_pair(
			current.thread_id, Thread{
				event.hThread,
				event.lpThreadLocalBase,
				event.lpStartAddress
			}
		));
	info("Started new thread TID(", current.thread_id, ")");
	return {};
}
GDBServerResponse GDBServer::handle_event(const CREATE_PROCESS_DEBUG_INFO& event) {
	auto process_id = GetProcessId(event.hProcess);
	auto thread_id = GetThreadId(event.hThread);
	processes.insert(std::make_pair(process_id, event.hProcess));
	threads.insert(std::make_pair(thread_id, Thread{
		event.hThread,
		event.lpBaseOfImage,
		event.lpStartAddress
	}));
	info("Started process PID(", process_id, ") TID(", thread_id, ")");
	return {};
}
GDBServerResponse GDBServer::handle_event(const EXIT_THREAD_DEBUG_INFO& event) {
	auto it = threads.find(current.thread_id);
	if(it != threads.end())
		threads.erase(it);
	info("Thread TID(", current.thread_id, ") exited with code ", event.dwExitCode);
	return {};
}
GDBServerResponse GDBServer::handle_event(const EXIT_PROCESS_DEBUG_INFO& event) {
	auto it = processes.find(current.process_id);
	if(it != processes.end())
		processes.erase(it);
	info("Process PID(", current.process_id, ") exited with code ", event.dwExitCode);
	return {};
}
GDBServerResponse GDBServer::handle_event(const LOAD_DLL_DEBUG_INFO& event) {
	void* debug_info = reinterpret_cast<void*>(
			reinterpret_cast<std::size_t>(event.lpBaseOfDll) + event.dwDebugInfoFileOffset
		);
	dlls.push_back({
		event.hFile,
		event.lpBaseOfDll,
		debug_info,
		static_cast<std::size_t>(event.nDebugInfoSize),
	});
	auto dll_name = try_get_dll_name(event.lpImageName, event.fUnicode != 0);
	if(dll_name) {
		info("Loaded new dll at ", event.lpBaseOfDll, " name: ", std::move(*dll_name));
	} else {
		info("Loaded new dll at ", event.lpBaseOfDll);
	}
	return {};
}
GDBServerResponse GDBServer::handle_event(const UNLOAD_DLL_DEBUG_INFO& event) {
	auto it = std::find_if(dlls.begin(), dlls.end(), [&event](const Dll& dll) { return dll.dll_base == event.lpBaseOfDll;});
	if(it != dlls.end()) {
		dlls.erase(it);
	}
	info("Dll at ", event.lpBaseOfDll, " unloaded.");
	return {};
}
GDBServerResponse GDBServer::handle_event(const OUTPUT_DEBUG_STRING_INFO& event) {
	return {};
}
GDBServerResponse GDBServer::handle_event(const RIP_INFO& event) {
	return {};
}
std::optional<std::string> GDBServer::try_get_dll_name(void* ptr, bool unicode) {
	wchar_t buffer[MAX_PATH];
	std::size_t written;
	if(!ReadProcessMemory(get_process_handle(current.process_id), ptr, buffer, MAX_PATH, &written)) {
		error("Cannot read dll name ", last_error());
		return {};
	}
	if(unicode) {
		auto wname = std::wstring(buffer);
		return std::string(wname.begin(), wname.end());
	}
	return std::string(reinterpret_cast<char*>(buffer));
}
GDBServerResponse GDBServer::handle_qSupported(std::string cmd) {
	GDBServerResponse response;
	Packet p('$', "PacketSize=47ff;QPassSignals+;QProgramSignals+;QStartupWithShell+;QEnvironmentHexEncoded+;QEnvironmentReset+;QEnvironmentUnset+;QSetWorkingDir+;QCatchSyscalls+;qXfer:libraries-svr4:read+;augmented-libraries-svr4-read+;qXfer:spu:read+;qXfer:spu:write+;qXfer:siginfo:read+;qXfer:siginfo:write+;qXfer:features:read+;QStartNoAckMode+;qXfer:osdata:read+;multiprocess+;fork-events+;vfork-events+;exec-events+;QNonStop+;QDisableRandomization+;qXfer:threads:read+;DisconnectedTracing+;qXfer:traceframe-info:read+;QTBuffer:size+;ConditionalBreakpoints+;BreakpointCommands+;QAgent+;Qbtrace-conf:bts:size+;Qbtrace-conf:pt:size+;swbreak+;hwbreak+;qXfer:exec-file:read+;vContSupported+;QThreadEvents+;no-resumed+");
	auto resp = p.to_string();
	response.packets.push_back(resp);
	return response;
}
GDBServerResponse GDBServer::handle_vMustReplyEmpty(std::string cmd) {
	return EMPTY_RESPONSE;
}
void GDBServerResponse::operator+=(GDBServerResponse&& response) {
	packets.insert(packets.end(),
				   std::make_move_iterator(response.packets.begin()),
				   std::make_move_iterator(response.packets.end()));
}
void GDBServerResponse::operator+=(Packet&& packet) {
	packets.emplace_back(packet.to_string());
}
void GDBServerResponse::operator+=(std::string&& str) {
	packets.emplace_back(str);
}
GDBServerResponse::GDBServerResponse(GDBServerResponse&& response) {
	packets = response.packets;
}
GDBServerResponse::GDBServerResponse(Packet&& packet) {
	packets = { packet.to_string() };
}
GDBServerResponse::GDBServerResponse(std::string&& str) {
	packets = { str };
}
GDBServerResponse::GDBServerResponse() {

}
GDBServerResponse GDBServer::handle_QStartNoAckMode(std::string cmd) {
	config.replay_ack = false;
	return OK_RESPONSE;
}
GDBServerResponse GDBServer::handle_QProgramSignals(std::string cmd) {
	if(cmd.length() < 1) {
		return EMPTY_RESPONSE;
	}
	cmd.erase(0, 1); // erase colon
	for_each_in_split(cmd, ';', [this](std::string value){
		this->config.signals_to_deliver.push_back(std::stoi(value, 0, 16));
	});
	return OK_RESPONSE;
}
GDBServerResponse GDBServer::handle_H(std::string cmd) {
	if(cmd.length() < 1) {
		return ERR_RESPONSE(00);
	}
	char op = cmd[0];
	cmd.erase(0, 1);
	auto pid_tid = parse_thread(cmd);
	if(!pid_tid) {
		return EMPTY_RESPONSE;
	}
	auto [ pids, tids] = std::move(*pid_tid);
	switch(op) {
		case 'c':
			process_thread_id.c.pids = pids;
			process_thread_id.c.tids = tids;
			break;
		case 'g':
			process_thread_id.g.pids = pids;
			process_thread_id.g.tids = tids;
			break;
		default:
			return EMPTY_RESPONSE;
	}
	return OK_RESPONSE;
}
std::optional<std::pair<std::vector<DWORD>, std::vector<DWORD>>> GDBServer::parse_thread(std::string& str) {
	if(str.length() < 1) {
		return {};
	}
	char with_process = str[0];
	if(with_process == 'p') {
		str.erase(0, 1);
		if(str.find('.') == std::string::npos) {
			int pid = std::stoi(str, 0, 16);
			auto pids = get_pids(pid);
			if(pids.size() == 0) {
				return {};
			}
			return std::make_pair(std::move(pids), get_tids(-1));
		} else {
			std::size_t ptr;
			int pid = std::stoi(str, &ptr, 16);
			str.erase(0, ptr+1);
			if(str == "") {
				return {};
			}
			int tid = std::stoi(str, 0, 16);
			auto tids = get_tids(tid);
			auto pids = get_pids(pid);
			if(pids.size() == 0 || tids.size() == 0) {
				return {};
			} else {
				return std::make_pair(std::move(pids), std::move(tids));
			}
		}
	} else {
		auto tids = get_tids(std::stoi(str, 0, 16));
		if(tids.size() == 0) {
			return {};
		}
		return std::make_pair(std::vector<DWORD>({current.process_id}), std::move(tids));
	}
}
std::vector<DWORD> GDBServer::get_pids(int pid) {
	if(pid < -1) {
		return {};
	}
	std::vector<DWORD> ret;
	switch(pid) {
		case -1:
			std::for_each(processes.begin(), processes.end(), [&ret](auto it){ret.push_back(it.first);});
			return ret;
			break;
		case 0: return { current.process_id }; break;
		default: return { static_cast<DWORD>(pid) };
	}
}
std::vector<DWORD> GDBServer::get_tids(int tid) {
	if(tid < -1) {
		return {};
	}
	std::vector<DWORD> ret;
	switch(tid) {
		case -1:
			std::for_each(threads.begin(), threads.end(), [&ret](auto it){ret.push_back(it.first);});
			return ret;
			break;
		case 0: return { current.thread_id }; break;
		default: return { static_cast<DWORD>(tid) };
	}
}
GDBServerResponse GDBServer::handle_qXfer_features_read_target_xml(std::string cmd) {
	std::size_t offset, size;
	if(std::sscanf(cmd.c_str(), "%llx,%llx", &offset, &size) != 2) {
		return EMPTY_RESPONSE;
	}
	return PARTIAL_RESPONSE(xml_target(), offset, size);
}
GDBServerResponse GDBServer::handle_QNonStop(std::string cmd) {
	config.non_stop_mode = (std::stoi(cmd) == 1);
	return OK_RESPONSE; 
}
GDBServerResponse GDBServer::generate_TStop(StopEvent event) {
	return Packet::gdb_general(
		concat(
			"T", 
			hexlify(event.signal_code), 
			serialize_register<Registers::Rip>(event.ctx), ";",
			serialize_register<Registers::Rsp>(event.ctx), ";",
			serialize_register<Registers::Rbp>(event.ctx), ";",
			"thread:", serialize_pid_tid(event.pid, event.tid), ";"
		)
	);
}
GDBServerResponse GDBServer::handle_stop_reason(std::string cmd) {
	if(stop_events.empty()) {
		return EMPTY_RESPONSE;
	} else {
		return generate_TStop(stop_events.front());
	}
}
GDBServerResponse GDBServer::handle_qXfer_threads_read(std::string cmd) {
	std::size_t offset, size;
	if(std::sscanf(cmd.c_str(), "%llx,%llx", &offset, &size) != 2) {
		return EMPTY_RESPONSE;
	}
	if(offset == 0) {
		update_modules();
		serialized_threads = generate_thread_list();
	}
	return PARTIAL_RESPONSE(serialized_threads, offset, size);
}
std::string GDBServer::generate_thread_list() {
	std::string threads;
	for(const auto &i : this->threads) {
		CONTEXT ctx;
		ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_DEBUG_REGISTERS;
		if(!GetThreadContext(i.second.handle, &ctx)) {
			error(THIS_PLACE, " cannot get thread context handle(", i.second.handle, ") ", last_error());
			continue;
		}
		std::optional<std::vector<Module>::const_iterator> mod;
		std::string pid_tid;
		for(const auto &process : modules) {
			auto module = std::find_if(process.second.begin(), process.second.end(), [&ctx](const Module& m){
				auto rip = reinterpret_cast<void*>(ctx.Rip);
				return m.code_start <= rip && m.code_end >= rip;
			});
			if(module != process.second.end()) {
				pid_tid = serialize_pid_tid(process.first, i.first);
				mod = module;
				break;
			}
		}
		if(mod) {
			threads += concat("<thread id=\"", pid_tid, "\" name=\"", (*mod)->name, "\"/>"); 
			mod.reset();
		} else {
			//threads += concat("<thread id=\"", , "\" />");
		}

	}
	return concat(THREADS(threads));
}
GDBServerResponse GDBServer::handle_qXfer_exec_file_read(std::string cmd) {
	DWORD pid;
	std::size_t offset;
	std::size_t size;
	if(std::sscanf(cmd.c_str(), "%x:%llx,%llx", &pid, &offset, &size) != 3) {
		return EMPTY_RESPONSE;
	}
	auto it = modules.find(pid);
	if(it == modules.end()) {
		update_modules();
	}
	it = modules.find(pid);
	if(it == modules.end()) { 
		error("Unknown process PID(", pid, ")");
		return EMPTY_RESPONSE;
	}
	return PARTIAL_RESPONSE(modules[pid].front().name, offset, size);
}
GDBServerResponse GDBServer::handle_qAttached(std::string cmd) {
	DWORD pid;
	if(std::sscanf(cmd.c_str(), ":%x", &pid) != 1) {
		return EMPTY_RESPONSE;
	}
	return STRING_RESPONSE("0");
}
#include "mdbg.hpp"
#include "params.hpp"
#include "packet.hpp"
#include "common.hpp"
#include <iostream>
#include "pe.hpp"
#include "log.hpp"
#include "ctf.hpp"

mDbg& mDbg::get_instance() {
    static mDbg debugger;
    return debugger;
}

bool mDbg::spawn_process(const std::string& exe_path, const std::string& child_dll) {
    STARTUPINFO si{sizeof(si)};
    BOOL is_created = CreateProcessA(
        NULL, (LPSTR) exe_path.c_str(),
        NULL, NULL,
        FALSE,
        DEBUG_ONLY_THIS_PROCESS|CREATE_SUSPENDED,
        NULL,
        NULL,
        &si, &pi
    );
    if(!is_created) {
        puts("Process creation error");
        return false;
    }
    if(!inject_dll(child_dll)) {
        error("Dll not inejcted");
        return false;
    }
    resolv_dbg_events();
    std::size_t entry_point = get_entry_point(pi.hProcess, file_name(exe_path));
    if(entry_point == 0) {
        return false;
    }
    set_initial_break(entry_point);
    return true;
}

void mDbg::set_initial_break(std::size_t entry_point) {
    HANDLE h_thread = OpenThread(THREAD_ALL_ACCESS, 0, pi.dwThreadId);
    HANDLE h_process = OpenProcess(PROCESS_ALL_ACCESS, 0, pi.dwProcessId);
    gdb_server.set_process_thread_id(pi.dwProcessId, pi.dwThreadId);
    gdb_server.print_regs();
    if(!gdb_server.set_hbreak(entry_point)) {
        error("initial break not set");
    }
    gdb_server.print_regs();
    if(ResumeThread(pi.hThread) == 1 ) { 
        info("Child resumed");
    }
}

bool mDbg::inject_dll(const std::string& child_dll) {
     LPVOID addr = VirtualAllocEx(pi.hProcess, NULL, 0x1000, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    if(!addr) {
        error(THIS_PLACE, "VirtualAlloc ", last_error());
        return false;
    }
    SIZE_T written;
    if(!WriteProcessMemory(pi.hProcess, addr, child_dll.c_str(), child_dll.length(), &written)) {
        error(THIS_PLACE, "WriteProcessMemory ", last_error());
        return false;
    }
    LPVOID load_library_a = GetProcAddress(GetModuleHandleA("kernel32"), "LoadLibraryA");
    HANDLE h_thread = CreateRemoteThread(pi.hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)load_library_a, addr, 0, NULL);
    if(h_thread == INVALID_HANDLE_VALUE) {
        error(THIS_PLACE, "CreateRemoteThread ", last_error());
        return false;
    }
    WaitForSingleObject(h_thread, INFINITY);
    VirtualFreeEx(pi.hProcess, addr, 0x1000, MEM_DECOMMIT|MEM_RELEASE);
    CloseHandle(h_thread);
    return true;
}

void mDbg::resolv_dbg_events() {
    DEBUG_EVENT ev;
    while(WaitForDebugEvent(&ev, 1000)) {
        /*
        switch(ev.dwDebugEventCode) {
            case EXCEPTION_DEBUG_EVENT: puts("EXCEPTION_DEBUG_EVENT"); break;
            case EXIT_PROCESS_DEBUG_EVENT: puts("EXIT_PROCESS_DEBUG_EVENT"); break;
            case CREATE_THREAD_DEBUG_EVENT: puts("CREATE_THREAD_DEBUG_EVENT"); break;
            case EXIT_THREAD_DEBUG_EVENT: puts("EXIT_THREAD_DEBUG_EVENT"); break;
            case CREATE_PROCESS_DEBUG_EVENT: puts("CREATE_PROCESS_DEBUG_EVENT"); break;
            case LOAD_DLL_DEBUG_EVENT: puts("LOAD_DLL_DEBUG_EVENT"); break;
            case UNLOAD_DLL_DEBUG_EVENT: puts("UNLOAD_DLL_DEBUG_EVENT"); break;
            default: printf("Crazy shit with %d debug event\n", ev.dwDebugEventCode);
        }
        */
        gdb_server.handle_event(ev);
        ContinueDebugEvent(ev.dwProcessId, ev.dwThreadId, DBG_CONTINUE);
    }
}

bool mDbg::init_networking(uint16_t port) {
    Net::InitNetworking();
    Server server_for_child{"127.0.0.1", DEFAULT_PORT};
    server_for_child.listen();
    auto conn_opt = server_for_child.accept();
    if(conn_opt) {
        info("Child connection accepted");
        child = std::move(*conn_opt);
        child->set_blocking(false);
    } else {
        error("Connection error", last_error());
        return false;
    }
    Server server_for_gdb{server_for_child.get_taskpool(), "0.0.0.0", port};
    server_for_gdb.listen();
    info("Waiting for remote gdb connection");
    auto remote_gdb_opt = server_for_gdb.accept();
    if(remote_gdb_opt) {
        gdb = std::move(*remote_gdb_opt);
        gdb->set_blocking(false);
        info("Accepted gdb client");
    } else {
        error("Connection to gdb client failed");
        return false;
    }
    return true;
}
std::size_t Console::read_from_source(std::back_insert_iterator<typename Base::Array> it) {
    DWORD num_of_events;
    if(!GetNumberOfConsoleInputEvents(h_console_input, &num_of_events)) {
        return 0;
    }
    if(num_of_events > 0) {
        const DWORD buffer_len = 0x100;
        INPUT_RECORD buffer[buffer_len];
        DWORD written;
        if(!ReadConsoleInput(h_console_input, buffer, buffer_len, &written)) {
            return 0;
        }
        std::size_t chars = 0;
        for(std::size_t i=0; i < written; ++i) {
            if((buffer[i].EventType == KEY_EVENT) && (buffer[i].Event.KeyEvent.bKeyDown == TRUE)) {
                it = buffer[i].Event.KeyEvent.uChar.AsciiChar;
                chars++;
            }
        }
        return chars;
    } else {
        return 0;
    }
}
bool Console::read_line(std::string& str) {
    for(auto chr = getc(); chr; chr = getc()) {
        if(*chr == '\r') {
            return true;
        } else if(*chr == '\x08') {
            if(str.length() > 0) {
                str.erase(--str.end());
            }
        } else {
            str += *chr;
        }
    }
    return false;
}
Console::Console(): Base(buffer) {
    h_console_input = GetStdHandle(STD_INPUT_HANDLE);
    if(h_console_input == INVALID_HANDLE_VALUE) {
        error(THIS_PLACE, " GetStdHandle ", last_error());
        throw std::runtime_error("invalid handle");
    }
}

void mDbg::handle_commandline(std::string commandline) {
    puts(commandline.c_str());
}
void mDbg::handle_gdb_packet(Packet& packet) {
    switch(packet.get_type()) {
        case Packet::Type::Ack: 
            if(!response_packets.empty()) {
                response_packets.pop_front();
            }
            packet.clear();
            break;
        case Packet::Type::NAck: 
            if(response_packets.empty()) {
                error("GDB NACKed but response queue is empty");
            } else {
                const auto & msg = response_packets.front();
                gdb->ostream.write(msg);
                if(!gdb->ostream.flush()) {
                    error("Cannot send packet to gdb");
                }
                gdb->ostream.clear();
            }
            packet.clear();
            break;
        case Packet::Type::GDB: 
            auto msg = packet.get_content();
            info("Got packet `", *msg, "`");
            handle_gdbserver_response(gdb_server.handle_request(std::move(msg)));
            break;
    }
}
void mDbg::handle_child_packet(Packet& packet) {
    puts((*packet.get_content()).c_str());
}
void mDbg::handle_gdbserver_response(GDBServerResponse response) {
    for(auto & i : response.packets) {
        info("resp `", i, "`");
        gdb->ostream.write(i);
        if(!gdb->ostream.flush()) {
            error("Cannot send packet to gdb");
        }
        gdb->ostream.clear();
        response_packets.push_back(std::move(i));
    }
}


void mDbg::run() {
    Packet gdb_packet;
    Packet packet_from_child;
    std::string comandline;
    for(;;) {
        if(console.read_line(comandline)) {
            handle_commandline(std::move(comandline));
        }
        if(gdb_packet.receive(gdb)) {
            handle_gdb_packet(gdb_packet);
        }
        if(packet_from_child.receive(child)) {
            handle_child_packet(packet_from_child);
        }
        //resolv_dbg_events();
        //handle_gdbserver_response()
        //gdb_server.print_regs();
        //getchar();
    }
}

#include "arch.hpp"

std::string serialize_pid_tid(DWORD pid, DWORD tid) {
	char buffer[sizeof("p00000000.00000000") + 1];
	if(std::snprintf(
		buffer,
		sizeof(buffer),
		"p%X.%X",
		pid,
		tid) < 0) {
		error(THIS_PLACE, "sprintf error");
		return "";
	}
	return std::string(buffer);
}

std::string serialize_registers(const CONTEXT& ctx) {
	return "";
}
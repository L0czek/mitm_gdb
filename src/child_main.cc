#define _WINSOCKAPI_
#include <Windows.h>
#include <iostream>
#include <iomanip>
#include "ctf.hpp"
#include "cdbg.hpp"
#include "hooks.hpp"
#include "log.hpp"
void main();

BOOL WINAPI DllMain(
  HINSTANCE hinstDLL,
  DWORD     fdwReason,
  LPVOID    lpvReserved
  ) {
	if(fdwReason == DLL_PROCESS_ATTACH){
		main();
	}
}

void main() {
    info("Hooking start");
    if(!cDbg::get_instance().init_networking()) {
        error("Networking not initiated");
    }
    info("Hooking end");
}

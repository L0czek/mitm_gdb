#include "pe.hpp"
#include "log.hpp"
#include <fstream>
#include <psapi.h>
#include "common.hpp"

std::size_t get_entry_point(HANDLE h_process,const std::string& name) {
    const std::size_t num_of_mods = 0x1000;
    HMODULE mods[num_of_mods];
    DWORD cb_needed;
    if(EnumProcessModules(h_process, mods, sizeof(mods), &cb_needed)) {
        for(std::size_t i=0; i < (cb_needed/sizeof(mods[0])); ++i) {
            char module_name[MAX_PATH];
            if(GetModuleFileNameExA(h_process, mods[i], module_name, sizeof(module_name) / sizeof(char))) {
                if(name == file_name<std::string>(module_name)) {
                    return reinterpret_cast<std::size_t>(mods[i]);
                }
            }
        }
    }
    return 0;
}

std::optional<std::map<std::string, std::size_t*>> parse_import_table(const std::string& dll_name) {
    HMODULE handle = GetModuleHandleA(dll_name.c_str());
    if(handle == INVALID_HANDLE_VALUE) {
        return {};
    }
    // TODO add win32 support
    auto dos_header = reinterpret_cast<IMAGE_DOS_HEADER*>(handle);
    auto nt_header = reinterpret_cast<IMAGE_NT_HEADERS64*>(
                reinterpret_cast<std::size_t>(handle) + dos_header->e_lfanew
            );
    auto import_directory = nt_header->OptionalHeader.DataDirectory[1];
    DWORD * import_ptr = reinterpret_cast<DWORD*>(
                reinterpret_cast<std::size_t>(handle) + import_directory.VirtualAddress
            );
    DWORD * import_end =  reinterpret_cast<DWORD*>(
                reinterpret_cast<std::size_t>(handle) + import_directory.VirtualAddress + import_directory.Size
            );
    std::map<std::string, std::size_t*> ret;
    for(; import_ptr < import_end; import_ptr += 5) {
        auto name_rva = import_ptr[0];
        auto addr_rva = import_ptr[4];
        if(name_rva == 0 || addr_rva == 0) {
            return ret;
        }
        auto name = reinterpret_cast<std::size_t*>(
                    reinterpret_cast<std::size_t>(handle) + name_rva
                );
        auto addr = reinterpret_cast<std::size_t*>(
                    reinterpret_cast<std::size_t>(handle) + addr_rva
                );
        while(*name) {
            if(*name & 0x80000000'00000000) {
                //TODO implement lookup by ordinal
            } else {
                // lookup by name
                auto name_ptr = reinterpret_cast<const char*>(
                            reinterpret_cast<std::size_t>(handle) + (*name & 0x7fffffff) + 2
                        );
                ret.insert(std::make_pair(std::string(name_ptr), reinterpret_cast<std::size_t*>(addr)));
            }
            name += 1;
            addr += 1;
        }
    }
    return ret;    
} 

std::pair<void*, void*> get_code_boundries(HANDLE h_process, void* ptr) {
    char headers[0x4000];
    SIZE_T read;
    if(!ReadProcessMemory(h_process, ptr, headers, sizeof(headers), &read)) {
        error("cannot read module headers");
        return std::make_pair(nullptr, nullptr);
    }
    auto dos_header = reinterpret_cast<IMAGE_DOS_HEADER*>(headers);
    auto nt_header = reinterpret_cast<IMAGE_NT_HEADERS64*>(
                reinterpret_cast<std::size_t>(headers) + dos_header->e_lfanew
            );
    return std::make_pair(
            reinterpret_cast<void*>(
                    reinterpret_cast<std::size_t>(ptr) + nt_header->OptionalHeader.BaseOfCode
                ),
            reinterpret_cast<void*>(
                    reinterpret_cast<std::size_t>(ptr) + nt_header->OptionalHeader.BaseOfCode + nt_header->OptionalHeader.SizeOfCode
                )
        );

}

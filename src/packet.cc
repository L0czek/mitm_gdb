#include "packet.hpp"
#include "common.hpp"

Packet::Packet() {
    state = StateOfRecv::Empty;
    type = Type::None; 
}

bool Packet::receive(Server::ConnectionPtr& connection) {
    for(auto chr = connection->istream.getc(); chr; chr = connection->istream.getc()) {
        switch(state) {
            case StateOfRecv::Empty: {
                                         if(*chr == '$' || *chr == '%') {
                                             type = Type::GDB;
                                             state = StateOfRecv::Content;
                                             type_char = *chr;
                                         } else if(*chr == '@') {
                                             type = Type::MGDB;
                                             state = StateOfRecv::Content;
                                         } else if(*chr == '+') {
                                            type = Type::Ack;
                                            state = StateOfRecv::Finished;
                                            checksum = '+';
                                            return true;
                                         } else if(*chr == '-') {
                                            type = Type::NAck;
                                            state = StateOfRecv::Finished;
                                            checksum = '-';
                                            return true;
                                         }
                                         break;
                                     }
            case StateOfRecv::Content: {
                                           if(*chr == '#') {
                                               state = StateOfRecv::Checksum1;
                                           } else {
                                               content += *chr;
                                           }
                                           break;
                                       }
            case StateOfRecv::Checksum1: {
                                            auto c = convert_char(*chr);
                                            if(c) {
                                                checksum = *c << 4;
                                                state = StateOfRecv::Checksum2;
                                            } else {
                                                clear();
                                            }
                                            break;
                                         }
            case StateOfRecv::Checksum2: {
                                             auto c = convert_char(*chr);
                                             if(c) {
                                                 checksum |= *c;
                                                 state = StateOfRecv::Finished;
                                                 return true;
                                             } else {
                                                 clear();
                                             }
                                             break;
                                         }
            case StateOfRecv::Finished: {
                                            return true;
                                        }
        }
    }
    return false;
}

std::optional<std::string> Packet::get_content() {
    if(state == StateOfRecv::Finished && is_valid()) {
        state = StateOfRecv::Empty;
        type = Type::None;
        return std::move(content);
    } else {
        return {};
    }
}

bool Packet::is_valid() const {
    return checksum == calc_checksum();
}

void Packet::clear() {
    type = Type::None;
    state = StateOfRecv::Empty;
    content.clear();
    checksum = 0;
}

std::string Packet::to_string() {
    const char* digits = "0123456789ABCDEF";
    std::string ret = concat(type_char, std::move(content), "#", digits[(checksum& 0xF0) >> 4], digits[(checksum & 0x0F) >> 0]);
    clear();
    return ret;
}

Packet::Packet(char type_char, std::string content): type_char(type_char), content(std::move(content)) {
    checksum = calc_checksum();
}

char Packet::calc_checksum() const {
    std::size_t sum = 0;
    std::for_each(content.begin(), content.end(), [&sum](char c) {
                sum += static_cast<unsigned char>(c);
            });
    return static_cast<char>(sum & 0xFF);
}

Packet::Type Packet::get_type() const {
    return type;
}
Packet Packet::gdb_general(std::string content) {
    return Packet('$', std::move(content));
}
Packet Packet::gdb_notification(std::string content) {
    return Packet('%', std::move(content));
}
Packet Packet::gdb_partial(const std::string& content, std::size_t offset, std::size_t size) {
    auto response = content.substr(offset, size);
    if(offset + size < content.length()) {
        return Packet::gdb_general(concat("m", std::move(response)));
    } else {
        return Packet::gdb_general(concat("l", std::move(response)));
    }
}
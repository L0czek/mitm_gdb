#include "cdbg.hpp"
#include "params.hpp"
#include "log.hpp"

cDbg& cDbg::get_instance() {
    static cDbg child_debugger;
    return child_debugger;
}

bool cDbg::init_networking() {
    Net::InitNetworking();
    Client cli;
    std::size_t num_of_tries = 0x1000;
    Client::ConnOpt conn_opt;
    do {
        conn_opt = cli.connect("127.0.0.1", DEFAULT_PORT);
        num_of_tries--;
    }while(!conn_opt && num_of_tries > 0);
    if(!conn_opt) {
        error("Cannot connect to parent");
        return false;
    } else {
        info("Child connected");
        connection = std::move(*conn_opt);
    }
    connection->ostream.write("@child-status:READY#00");
    connection->ostream.flush();
    return true;
}

void cDbg::debugger_main(HANDLE h_process, HANDLE h_thread, DWORD continue_status) {
    
}


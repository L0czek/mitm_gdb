#include "ctf.hpp"

#include <cstring>
#include <Windows.h>
#include <wtsapi32.h>

u32 getPid(const char* name) {
	WTS_PROCESS_INFO* wProcInfo;
	DWORD procCount;
	if(WTSEnumerateProcessesA(WTS_CURRENT_SERVER_HANDLE, 0, 1, &wProcInfo, &procCount)){
		for(u32 i=0; i < procCount; ++i){
			if(!strcmp(name, wProcInfo[i].pProcessName)){
				u32 pid = wProcInfo[i].ProcessId;
				WTSFreeMemory(wProcInfo);
				return pid;
			}
		}
	}
	if(wProcInfo){
		WTSFreeMemory(wProcInfo);
	}
	return 0;
}

Pipe::Pipe(const char* pipeName) {
		hPipe = CreateNamedPipeA(
			pipeName,				  // pipe name
			PIPE_ACCESS_DUPLEX,       // read/write access 
			PIPE_TYPE_MESSAGE |       // message type pipe 
			PIPE_READMODE_MESSAGE |   // message-read mode 
			PIPE_WAIT,                // blocking mode 
			PIPE_UNLIMITED_INSTANCES, // max. instances  
			BUFFER_SIZE,              // output buffer size 
			BUFFER_SIZE,              // input buffer size 
			NMPWAIT_USE_DEFAULT_WAIT, // client time-out 
			NULL                   // default security attribute 
		);
		if(hPipe == INVALID_HANDLE_VALUE ){
			throw std::runtime_error("Broken Pipe");
		}
		bool isClientConected = ConnectNamedPipe(hPipe, NULL);
		if(!isClientConected){
			throw std::runtime_error("Broken Pipe");
		}
	}
void Pipe::puts(const char* str) const{
	std::size_t len = strlen(str);
	std::size_t i = 0;
	DWORD read = 0;
	if(len > BUFFER_SIZE){
		for(; len - i > BUFFER_SIZE; i += BUFFER_SIZE){
			if(!WriteFile(
				hPipe,
				&str[i],
				BUFFER_SIZE,
				&read,
				NULL
			)){
				throw std::runtime_error("Error while writing");
			}
		}
	}
	if(!WriteFile(
		hPipe,
		&str[i],
		len - i,
		&read,
		NULL
	)){
		throw std::runtime_error("Error while writing");
	}
}
	
void interactWithPipe(const char* name) {
	HANDLE hPipe;
	std::size_t tries = 0;
	do{
		hPipe = CreateFile( 
          name,   // pipe name 
          GENERIC_READ |  // read and write access 
          GENERIC_WRITE, 
          0,              // no sharing 
          NULL,           // default security attributes
          OPEN_EXISTING,  // opens existing pipe 
          0,              // default attributes 
          NULL            // no template file
		);
		tries++;
	}
	while(hPipe == INVALID_HANDLE_VALUE && tries < 1'000'000);
	if(hPipe == INVALID_HANDLE_VALUE){
		throw std::runtime_error("Broken pipe");
	}
	puts("Pipe opened");
	char buffer[BUFFER_SIZE+1];
	DWORD read = 0;
	while(true){
		if(ReadFile(
			hPipe,
			buffer,
			sizeof(buffer),
			&read,
			NULL
		)){
			if(read > 0){
				buffer[read] = '\0';
				fwrite(buffer, sizeof(char), read, stdout);
				fflush(stdout);
			}
		}else{
			break;
		}
	}
}
void Pipe::printf(const char* format, ...) const{
	va_list list;
	va_start(list, format);
	char buffer[0x1000+1];
	long long required_size = std::vsnprintf(buffer, 0x1000, format, list);
	if(required_size > 0) {
		Pipe::puts(buffer);
	}
}

HANDLE injectDll(const char* processName, const char* dllName) {
	auto pid = getPid(processName);
	if(pid == 0){
		printf("No such process %s\n", processName);
		return INVALID_HANDLE_VALUE;
	}
	return injectDll(pid, dllName);
}

HANDLE injectDll(u32 pid, const char* dllName) {
	HANDLE hproc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if(hproc == INVALID_HANDLE_VALUE){
		printf("Error opening process");
		return INVALID_HANDLE_VALUE;
	}
	std::size_t len = strlen(dllName);
	LPVOID addr = VirtualAllocEx(
		hproc,
		NULL,
		len,
		MEM_COMMIT|MEM_RESERVE,
		PAGE_READWRITE
	);
	if(!addr){
		printf("Error on allocating memory");
		return INVALID_HANDLE_VALUE;
	}
	DWORD written = 0;
	if(!WriteProcessMemory(
		hproc,
		addr,
		dllName,
		len,
		NULL
	)){
		printf("Error on WriteProcessMemory");
		return INVALID_HANDLE_VALUE;
	}
	LPVOID hLoadLibraryA = GetProcAddress(GetModuleHandleA("kernel32"), "LoadLibraryA");
	HANDLE hthread = CreateRemoteThread(
		hproc,
		NULL,
		0,
		(LPTHREAD_START_ROUTINE)hLoadLibraryA,
		addr,
		0,
		NULL
	);
	if(hthread == INVALID_HANDLE_VALUE){
		printf("Error on CreateRemoteThread\n");
		return INVALID_HANDLE_VALUE;
	}
	return hproc;
}
HANDLE spawnProcessSuspendedWithDll(const char* exeName, const char* dllName) {
	STARTUPINFO info={sizeof(info)};
	PROCESS_INFORMATION processInfo;
	if(CreateProcessA(
		NULL,
		(LPSTR)exeName,
		NULL,
		NULL,
		TRUE,
		CREATE_SUSPENDED,
		NULL,
		NULL,
		&info,
		&processInfo
	)){
		return injectDll(processInfo.dwProcessId, dllName);
	}else{
		printf("Error on CreateProcessA\n");
		return 0;
	}
}
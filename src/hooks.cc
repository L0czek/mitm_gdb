#include "hooks.hpp"
#include "pe.hpp"
#include <cstring>
#include "cdbg.hpp"

// WARINING lots of `C with classes` ahead

bool hooks_enabled = false;

NTSTATUS dbg_ui_continue_retval;
NTSTATUS dbg_ui_wait_state_change_retval;
NTSTATUS dbg_ui_convert_state_change_structure_retval;
DEBUG_EVENT event_to_replay;
char wait_state_buffer[0x80];

HANDLE h_process = INVALID_HANDLE_VALUE;
HANDLE h_thread = INVALID_HANDLE_VALUE;

NTSTATUS (__stdcall*dbg_ui_continue_unhooked)(PCLIENT_ID AppClientId, NTSTATUS ContinueStatus);
NTSTATUS (__stdcall*dbg_ui_wait_state_change_unhooked)(PDBGUI_WAIT_STATE_CHANGE StateChange, PLARGE_INTEGER Timeout);
NTSTATUS (__stdcall*dbg_ui_convert_state_change_structure_unhooked)(PDBGUI_WAIT_STATE_CHANGE StateChange, struct _DEBUG_EVENT * DebugEvent);	
NTSTATUS (__stdcall*nt_create_user_process_unhooked)(
    PHANDLE ProcessHandle,
    PHANDLE ThreadHandle,
    ACCESS_MASK ProcessDesiredAccess,
    ACCESS_MASK ThreadDesiredAccess,
    POBJECT_ATTRIBUTES ProcessObjectAttributes,
    POBJECT_ATTRIBUTES ThreadObjectAttributes,
    ULONG ProcessFlags,
    ULONG ThreadFlags,
    PRTL_USER_PROCESS_PARAMETERS ProcessParameters,
    PPROCESS_CREATE_INFO CreateInfo,
    PPROCESS_ATTRIBUTE_LIST AttributeList
    );

NTSTATUS __stdcall dbg_ui_continue_hook(PCLIENT_ID AppClientId, NTSTATUS ContinueStatus) {
    if(hooks_enabled) {
        HANDLE h_process = AppClientId->UniqueProcess;
        HANDLE h_thread = AppClientId->UniqueThread;
        disable_hooks();
        cDbg::get_instance().debugger_main(h_process, h_thread, ContinueStatus);
        enable_hooks();
        return dbg_ui_continue_retval;
    } else {
        NTSTATUS ret = dbg_ui_continue_unhooked(AppClientId, ContinueStatus);
        dbg_ui_continue_retval = ret;
        return ret;
    }
}
NTSTATUS __stdcall dbg_ui_wait_state_change_hook(PDBGUI_WAIT_STATE_CHANGE StateChange, PLARGE_INTEGER Timeout) {
    if(hooks_enabled) {
        memcpy(StateChange, wait_state_buffer, sizeof(wait_state_buffer)); 
        return dbg_ui_wait_state_change_retval;
    } else {
        NTSTATUS ret = dbg_ui_wait_state_change_unhooked(StateChange, Timeout);
        if(ret != 257 && ret != 192) {
            memcpy(wait_state_buffer, StateChange, sizeof(wait_state_buffer));
            dbg_ui_wait_state_change_retval = ret;
        } 
        return ret;
    }
}
NTSTATUS __stdcall dbg_ui_convert_state_change_structure_hook(PDBGUI_WAIT_STATE_CHANGE StateChange, struct _DEBUG_EVENT * DebugEvent) {
    if(hooks_enabled) {
        memcpy(DebugEvent, &event_to_replay, sizeof(DEBUG_EVENT));
        return dbg_ui_convert_state_change_structure_retval;
    } else {
        NTSTATUS ret = dbg_ui_convert_state_change_structure_unhooked(StateChange, DebugEvent);
        memcpy(&event_to_replay, DebugEvent, sizeof(DEBUG_EVENT));
        dbg_ui_convert_state_change_structure_retval = ret;
        return ret;
    }
}	
NTSTATUS __stdcall nt_create_user_process_hook(
    PHANDLE ProcessHandle,
    PHANDLE ThreadHandle,
    ACCESS_MASK ProcessDesiredAccess,
    ACCESS_MASK ThreadDesiredAccess,
    POBJECT_ATTRIBUTES ProcessObjectAttributes,
    POBJECT_ATTRIBUTES ThreadObjectAttributes,
    ULONG ProcessFlags,
    ULONG ThreadFlags,
    PRTL_USER_PROCESS_PARAMETERS ProcessParameters,
    PPROCESS_CREATE_INFO CreateInfo,
    PPROCESS_ATTRIBUTE_LIST AttributeList
    ) {
    NTSTATUS ret = nt_create_user_process_unhooked(ProcessHandle, ThreadHandle, ProcessDesiredAccess, ThreadDesiredAccess, ProcessObjectAttributes, ThreadObjectAttributes, ProcessFlags, ThreadFlags, ProcessParameters, CreateInfo, AttributeList);
    if(ret == 0) { // 0 => STATUS_SUCCESS
        h_process = *ProcessHandle;
        h_thread = *ThreadHandle;
    }
    return ret;
}
void enable_hooks() {
    hooks_enabled = true;
}
void disable_hooks() {
    hooks_enabled = false;
}
bool init_hooks() {
    auto import_table_opt = parse_import_table("kernel32");
    if(!import_table_opt) {
        return false;
    }
    auto import_table = std::move(*import_table_opt);
    std::size_t* dbg_ui_continue_addr = import_table["DbgUiContinue"];
    std::size_t* dbg_ui_wait_state_change_addr = import_table["DbgUiWaitStateChange"];
    std::size_t* dbg_ui_convert_state_change_structure_addr = import_table["DbgUiConvertStateChangeStructure"];
    std::size_t* nt_create_user_process_addr = import_table["NtCreateUserProcess"];
    if(!dbg_ui_continue_addr || !dbg_ui_wait_state_change_addr || !dbg_ui_convert_state_change_structure_addr || !nt_create_user_process_addr) {
        return false;
    }
    dbg_ui_continue_unhooked = reinterpret_cast<NTSTATUS (*)(PCLIENT_ID AppClientId, NTSTATUS ContinueStatus)>(*dbg_ui_continue_addr);
    dbg_ui_wait_state_change_unhooked = reinterpret_cast<NTSTATUS (*)(PDBGUI_WAIT_STATE_CHANGE StateChange, PLARGE_INTEGER Timeout)>(*dbg_ui_wait_state_change_addr);
    dbg_ui_convert_state_change_structure_unhooked = reinterpret_cast<NTSTATUS (*)(PDBGUI_WAIT_STATE_CHANGE StateChange, struct _DEBUG_EVENT * DebugEvent)>(*dbg_ui_convert_state_change_structure_addr);
    nt_create_user_process_unhooked = reinterpret_cast<NTSTATUS (__stdcall*)(PHANDLE, PHANDLE, ACCESS_MASK, ACCESS_MASK, POBJECT_ATTRIBUTES, POBJECT_ATTRIBUTES ,
    ULONG, ULONG, PRTL_USER_PROCESS_PARAMETERS, PPROCESS_CREATE_INFO, PPROCESS_ATTRIBUTE_LIST)>(*nt_create_user_process_addr);
    if( !patch(dbg_ui_continue_addr, reinterpret_cast<std::size_t>(dbg_ui_continue_hook)) ||
        !patch(dbg_ui_wait_state_change_addr, reinterpret_cast<std::size_t>(dbg_ui_wait_state_change_hook)) ||
        !patch(dbg_ui_convert_state_change_structure_addr, reinterpret_cast<std::size_t>(dbg_ui_convert_state_change_structure_hook)) ||
        !patch(nt_create_user_process_addr, reinterpret_cast<std::size_t>(nt_create_user_process_hook))) {
        return false;
    }
    return true;
}

bool patch(std::size_t* ptr, std::size_t value) {
    DWORD old_protections;
    if(!VirtualProtect(ptr, 8, PAGE_READWRITE, &old_protections)) {
        return false;
    }
    *ptr = value;
    DWORD tmp;
    VirtualProtect(ptr, 8, old_protections, &tmp);
    return true;
}

std::pair<HANDLE, HANDLE> get_process_handles() {
    return std::make_pair(h_process, h_thread);
}